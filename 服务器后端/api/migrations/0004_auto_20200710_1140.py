# Generated by Django 2.2 on 2020-07-10 11:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20200710_0931'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attachment',
            name='file_name',
        ),
        migrations.RemoveField(
            model_name='attachment',
            name='hetong',
        ),
        migrations.RemoveField(
            model_name='attachment',
            name='lujin',
        ),
        migrations.AddField(
            model_name='attachment',
            name='file',
            field=models.FileField(default=1, upload_to='files/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contract_info',
            name='attachment',
            field=models.ManyToManyField(null=True, to='api.attachment', verbose_name='附件'),
        ),
        # migrations.CreateModel(
        #     name='Account_view',
        #     fields=[
        #         ('type', models.CharField(max_length=2)),
        #         ('sysid', models.CharField(max_length=64, primary_key=True, serialize=False)),
        #         ('tableid', models.IntegerField(max_length=32)),
        #         ('cause', models.CharField(max_length=64)),
        #         ('money', models.DecimalField(decimal_places=2, max_digits=10)),
        #         ('account_date', models.DateTimeField(auto_now_add=True, null=True)),
        #         ('finish_flag', models.IntegerField()),
        #         ('expense_name', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.UserInfo')),
        #         ('project_name', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='api.Project_info')),
        #     ],
        #     options={
        #         'db_table': 'account_view',
        #     },
        # ),
    ]
